package br.com.sisnema.financeiroweb.util;

public class DAOException extends Exception {

	private static final long serialVersionUID = -4238826841672417507L;

	public DAOException() {
		super();
	}

	public DAOException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

}
