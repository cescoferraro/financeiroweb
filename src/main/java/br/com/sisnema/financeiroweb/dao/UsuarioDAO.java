package br.com.sisnema.financeiroweb.dao;

import br.com.sisnema.financeiroweb.model.Usuario;
import br.com.sisnema.financeiroweb.util.DAOException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import javax.persistence.OptimisticLockException;
import java.util.List;

public class UsuarioDAO extends DAO<Usuario> {

	public void atualizar(Usuario model) throws DAOException {
        try {
            getSession().update(model);
            getSession().getTransaction().commit();
        } catch (OptimisticLockException e) {
            throw new DAOException("este registro acaba de ser atualizado" +
                    " por outro usuario. Refaça a sua pesquisa!");
        }
    }

	public void salvar(Usuario model) throws DAOException {
		getSession().save(model);
	}

	public void excluir(Usuario model) throws DAOException {
		getSession().delete(model);
	}

	public Usuario obterPorId(Usuario filtro) {
		return getSession().get(Usuario.class, filtro.getCodigo());
	}

	public Usuario buscarPorLogin(String login) {
		Criteria criteria = getSession().createCriteria(Usuario.class);
		criteria.add(Restrictions.eq("login", login));
		return (Usuario) criteria.uniqueResult();
	}

	public Usuario buscarPorLoginESenha(String login, String senha) {
		Criteria criteria = getSession().createCriteria(Usuario.class);
		criteria.add(Restrictions.eq("login", login));
		criteria.add(Restrictions.eq("senha", senha));
		return (Usuario) criteria.uniqueResult();
	}

	public List<Usuario> pesquisar(Usuario filtros) {
		Criteria criteria = getSession().createCriteria(Usuario.class);

		if(StringUtils.isNotBlank(filtros.getNome())){
			criteria.add(Restrictions.ilike("nome", filtros.getNome(), MatchMode.ANYWHERE));
		}

		criteria.addOrder(Order.asc("nome"));

		return criteria.list();
	}

}
