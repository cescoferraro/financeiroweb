package br.com.sisnema.financeiroweb.action;

import br.com.sisnema.financeiroweb.model.BaseEntity;
import br.com.sisnema.financeiroweb.negocio.IRN;
import br.com.sisnema.financeiroweb.util.RNException;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public abstract class ActionBean<T extends BaseEntity> {
	
	protected final IRN<T> negocio;

	public ActionBean(IRN<T> rn) {
		super();
		this.negocio = rn;
	}

	protected final void apresentarMensagemDeSucesso(String msg) {
		apresentarMensagem(FacesMessage.SEVERITY_INFO, msg);
	}
	
	protected final void apresentarMensagemDeErro(String msg) {
		apresentarMensagem(FacesMessage.SEVERITY_ERROR, msg);
	}
	
	protected final void apresentarMensagem(RNException e) {
		apresentarMensagem(FacesMessage.SEVERITY_ERROR, e.getMessage());
	}
	
	protected final void apresentarMensagem(Severity severity, String msg) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, msg, ""));
	}
}
