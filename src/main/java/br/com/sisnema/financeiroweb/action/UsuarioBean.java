package br.com.sisnema.financeiroweb.action;

import br.com.sisnema.financeiroweb.model.Usuario;
import br.com.sisnema.financeiroweb.negocio.UsuarioRN;
import br.com.sisnema.financeiroweb.util.RNException;
import org.apache.commons.lang3.StringUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean
@RequestScoped
public class UsuarioBean extends ActionBean<Usuario> {

	public UsuarioBean() {
		super(new UsuarioRN());
	}

	private Usuario usuario = new Usuario();
	private String confirmaSenha;
	private List<Usuario> lista;
	private String destinoSalvar;


	public String novo() {
		destinoSalvar = "/publico/usuarioSucesso";
		usuario = new Usuario();
		usuario.setAtivo(true);
		return "/publico/usuario";
	}

	public String editar() {
		destinoSalvar = "/admin/principal";
		confirmaSenha = usuario.getSenha();
		return "/publico/usuario";
	}

	public String excluir(){
		try {
			String msg = "Usu�rio "+usuario.getNome()+" excluido com sucesso";
			negocio.excluir(usuario);
			apresentarMensagemDeSucesso(msg);
			usuario = new Usuario();
			lista = null;
		} catch (RNException e) {
			apresentarMensagem(e);
		}

		return null;
	}

	public String salvar(){

		try {
			if(!StringUtils.equals(usuario.getSenha(), confirmaSenha)){
				apresentarMensagemDeErro("Senhas diferentes");
				return null;
			}

			String operacao = usuario.getCodigo() == null ? "inserido" : "alterado";
			String msg = "Usuario "+operacao+" com sucesso";

			negocio.salvar(usuario);
			apresentarMensagemDeSucesso(msg);
			usuario = new Usuario();
			lista = null;

			return destinoSalvar;

		} catch (RNException e) {
			apresentarMensagem(e);
		}

		return null;
	}

	public String ativar(){
//		try {
//			if(usuario.isAtivo()){
//				usuario.setAtivo(false);
//			} else {
//				usuario.setAtivo(true);
//			}
//
//			negocio.salvar(usuario);
//
//		} catch (RNException e) {
//			apresentarMensagem(e);
//		}
	usuario.setAtivo(!usuario.isAtivo());
			return null;
	}

	public List<Usuario> getLista() {
		if(lista == null){
			lista = negocio.pesquisar(usuario);
		}

		return lista;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getConfirmaSenha() {
		return confirmaSenha;
	}

	public void setConfirmaSenha(String confirmaSenha) {
		this.confirmaSenha = confirmaSenha;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public String getDestinoSalvar() {
		return destinoSalvar;
	}

	public void setDestinoSalvar(String destinoSalvar) {
		this.destinoSalvar = destinoSalvar;
	}


}
