package br.com.sisnema.financeiroweb.negocio;

import java.util.List;

import br.com.sisnema.financeiroweb.dao.UsuarioDAO;
import br.com.sisnema.financeiroweb.model.Usuario;
import br.com.sisnema.financeiroweb.util.DAOException;
import br.com.sisnema.financeiroweb.util.RNException;

public class UsuarioRN extends RN<Usuario> {

	public UsuarioRN() {
		super(new UsuarioDAO());
	}

	public void salvar(Usuario model) throws RNException {
		if(model.getCodigo() == null){
			try {
				
				// validar se j� existe um usu�rio com o login
				Usuario usuarioExistente = buscarPorLogin(model.getLogin());
				if(usuarioExistente != null){
					throw new RNException("J� existe um usu�rio com o login informado");
				}
				
				dao.salvar(model);
				
			} catch (DAOException e) {
				throw new RNException("N�o foi possivel inserir. Erro: "+e.getMessage(), e);
			}
		} else {
			try {
				((UsuarioDAO) dao).atualizar(model);
			} catch (DAOException e) {
				throw new RNException("N�o foi possivel atualizar. Erro: "+e.getMessage(), e);
			}
		}
	}

	public void excluir(Usuario model) throws RNException {
		try {
			dao.excluir(model);
		} catch (DAOException e) {
			throw new RNException("N�o foi possivel excluir. Erro: "+e.getMessage(), e);
		}
	}

	public Usuario obterPorId(Usuario filtro) {
		return dao.obterPorId(filtro);
	}

	public List<Usuario> pesquisar(Usuario filtros) {
		return dao.pesquisar(filtros);
	}

	public Usuario buscarPorLogin(String login) {
		UsuarioDAO userDAO = (UsuarioDAO) dao;
		return userDAO.buscarPorLogin(login);
	}
	
	public Usuario buscarPorLoginESenha(String login, String senha) {
		return ( (UsuarioDAO) dao ).buscarPorLoginESenha(login, senha);
	}

}















