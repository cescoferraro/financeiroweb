package br.com.sisnema.financeiroweb.model;

import javax.persistence.MappedSuperclass;
import javax.persistence.PostUpdate;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by cesco on 10/8/15.
 */

@MappedSuperclass
public class BaseEntity implements Serializable {


    private static final long serialVersionUID = -3182717036096566804L;

    @Version
    protected int version;

    protected Date dtCreated;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseEntity)) return false;

        BaseEntity that = (BaseEntity) o;

        if (version != that.version) return false;
        if (!dtCreated.equals(that.dtCreated)) return false;
        return dtUpdated.equals(that.dtUpdated);

    }

    @Override
    public int hashCode() {
        int result = version;
        result = 31 * result + dtCreated.hashCode();
        result = 31 * result + dtUpdated.hashCode();
        return result;
    }

    protected Date dtUpdated;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    public Date getDtUpdated() {
        return dtUpdated;
    }

    public void setDtUpdated(Date dtUpdated) {
        this.dtUpdated = dtUpdated;
    }

    @PreUpdate
    void onPesist(){
        dtCreated = new Date();
        dtUpdated = new Date();
    }

    @PostUpdate
    void onUpdate(){
        dtUpdated = new Date();
    }

}
